# Desafio Adriana Hernandez

## Tutorial para deploy no docker

<p>
Após clonar o projeto, executar o comando na raiz: <br>

- docker-compose up -d --build database && docker-compose up -d --build app && docker-compose up -d --build web 

</p>
<p>
Em seguida, insira o container docker do laravel com o comando:<br>

- docker exec -it laravel_app bash<br>

E execute os comandos do laravel:<br>
- php artisan key:generate<br>
- php artisan config:cache<br>
- php artisan route:cache<br>
</p>

## Configuração do Banco de Dados

<p>Para criar o banco de dados, deve-se abrir a CLI do container do mysql e executar os seguintes comandos:<br>

- mysql -u root -p<br>
- A senha é 123456
- CREATE DATABASE desafioBD;<br>
- use desafioDB;<br>

Após estar no database desafioBD, cole o script do banco que se econtra na raiz do projeto (desafioBD.sql), para criação de tabelas e conteúdo inicial.
</p>


## Uma vez iniciado, você pode abrir seu navegador em:

**localhost:8990**

## Login no sistema como admin:

Email: admin@admin.com<br>
Senha: 123456


