/* scripts páginas admin */

  function show(div){
    //$('#'+div).addClass('active');
    $('.option').css('display','none');
    $('.'+div).css('display','flex');
  }

  function openModalInsert(){
    $('#modalInsert').modal();
  }
  function openModal(id, state, value_kwh){
      
    //document.getElementById('basicModal').style.display = 'block';
    $('#largeModal').modal();
    document.getElementById('state_update').value = state;
    document.getElementById('value_kwh_update').value = value_kwh;
    document.getElementById('id_state_update').value = id;
  }

  function openModalDelete(id){
    $('#basicModalDelete').modal();
    document.getElementById('basicModalDelete').style.display = 'block';
    document.getElementById('id_state').value = id;
    
  }

  function updateValueGenerator(){
    value = document.getElementById('value_generator').value;
    localStorage.setItem('valueKwh', value);
  }

	$(document).ready(function() {
    
    feather.replace();
  
    $("#form_add_state").submit(function (event) {
        event.preventDefault();
        let form = $(this);
        let url = form.attr('action');

        let param = {

            state: $("#state").val(),
            value_kwh: $("#value_kwh").val(),
            _token: $("#token").val()
        };

        let posting = $.post(url, param);
        posting.done(function (data) {

            if (data['status'] == 'success') {
                $('#modalInsert').modal('hide');
                 $('#form_add_state').each(function () {
                    this.reset();
                });
                var trHTML = '';
                id = data["id"];
                
                trHTML += '<tr id="'+id+'"><td>'+param.state+'</td><td>' + param.value_kwh + '</td><td><a onclick="openModalDelete('+id+')" href="#"><i data-feather="trash-2" width="18"></i></a><a onclick=\'openModal('+id+',"'+param.state+'", '+param.value_kwh+')\' id="'+param.state+'" href="#"> <i data-feather="edit" width="18"></i></a></td></tr>';
                $('#table_states').append(trHTML);

                feather.replace();

            }else{
              alert(data['status']);
            } 


        });
    });

    $("#form_update_state").submit(function (event) {
      event.preventDefault();
        let form = $(this);
        let url = form.attr('action');

        let param = {

          id_state_update: $("#id_state_update").val(),
          state_update: $("#state_update").val(),
          value_kwh_update: $("#value_kwh_update").val(),
          _token: $("#token").val()
        };

        let posting = $.post(url, param);
        posting.done(function (data) {

            if (data['status'] == 'success') {
                $('#largeModal').modal('hide');
                location.reload();
            }else{
              alert('erro');
            } 

        });
    });



    $("#form_delete").submit(function (event) {
        event.preventDefault();
        let form = $(this);
        let url = form.attr('action');

        let param = {
          id_state: $("#id_state").val(),
          _token: $("#token").val()
        };

        let posting = $.post(url, param);
        posting.done(function (data) {

            if (data) {
                $('#basicModalDelete').modal('hide');
                $('#'+param.id_state).remove();
            } 

        });
    });

    $("#form_update_generator").submit(function (event) {
      event.preventDefault();
        let form = $(this);
        let url = form.attr('action');

        let param = {

          value_generator: $("#value_generator").val(),
          _token: $("#token").val()
        };

        let posting = $.post(url, param);
        posting.done(function (data) {
          
          if (data['status'] == 'success') {
            alert('Atualizado com sucesso');
          }else{
            alert(data['status']);
          }

        });
    });

    /* scripts página cliente */
    $("#form_simulation").submit(function (event) {
        event.preventDefault();

        let form = $(this);
        let url = form.attr('action');

        let param = {

            name: $("#name").val(),
            email: $("#email").val(),
            phone: $("#phone").val(),
            address: $("#address").val(),
            state: $("#state").val(),
            value_kwh: $("#value_kwh").val(),
            _token: $("#token").val()
        };
        
        let posting = $.post(url, param);
        posting.done(function (data) {
            
            if (data['status'] == 'success') {
               
                $('.modal-result-simulation').modal();
                $('#result_simulation').text('R$ '+ data['value_generator_format']);
                $('#result_time').text(data['value_time']);

            } else {
                alert('erro');
            }


        });
    });



});
	