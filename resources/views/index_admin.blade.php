@extends('main')
@section('content')

    <div class="container-full flex-column admin">
        @if (!Auth::guest())
            <div class="container nav">
                <nav class="navbar navbar-light bg-light menu">
                    <a class="navbar-brand" id="table-states" href="#" onclick="show('table-states')">Gerenciar Estados</a>
                    <a class="navbar-brand" id="table-simulations" href="#" onclick="show('table-simulations')">Visualizar
                        Simulações</a>
                    <a class="navbar-brand" id="value-generator" href="#" onclick="show('value-generator')">Atualizar Custo
                        Gerador</a>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                        style="margin: 11px;">
                        <i data-feather='log-out' width="20"></i>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </nav>
            </div>



            <div class="flex-column container">

                <div class="option table-states title-admin">
                    <h1>Estados</h1>
                    <p> Gerencie e insira novos estados</p>
                </div>

                <div class="option table-states">
                    <table class="table" id="table_states">
                        <thead>
                            <tr>
                                <th scope="col">Estado</th>
                                <th scope="col">Valor KWH</th>
                                <th scope="col">Opções</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $state)
                                <tr id="{{ $state->id }}">
                                    <td id="{{ $state->id }}_td">{{ $state->state }}</td>
                                    <td id="{{ $state->value_kwh }}_kwh">{{ $state->value_kwh }}</td>
                                    <td>
                                        <a onclick="openModalDelete('{{ $state->id }}')"><i data-feather="trash-2"
                                                width="18"></i></a>
                                        <a onclick="openModal('{{ $state->id }}', '{{ $state->state }}', '{{ $state->value_kwh }}')"
                                            id="{{ $state->state }}"> <i data-feather="edit" width="18"></i></a>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>

                    <input class="btn btn-black" type="button" onclick="openModalInsert()" value="Adicionar um estado">
                </div>

                <div class="option table-simulations title-admin">
                    <h1>Simulações</h1>
                    <p> Consulte as simulações realizadas</p>
                </div>
                <div class="option table-simulations">

                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Nome</th>
                                <th scope="col">Email</th>
                                <th scope="col">Endereço</th>
                                <th scope="col">Telefone</th>
                                <th scope="col">Estado</th>
                                <th scope="col">Valor mensal conta</th>
                                <th scope="col">Valor da simulação</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data_simulations as $simulation)
                                <tr>
                                    <td>{{ $simulation->name }}</td>
                                    <td>{{ $simulation->email }}</td>
                                    <td>{{ $simulation->address }}</td>
                                    <td>{{ $simulation->phone }}</td>
                                    <td>{{ $simulation->state }}</td>
                                    <td>{{ $simulation->value_kwh }}</td>
                                    <td>{{ $simulation->value_generator }}</td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>

                <div class="option value-generator title-admin">
                    <h1>Custo do gerador por KWh</h1>

                    <p>Atualize o custo do gerador</p>
                </div>
                <div class="option value-generator">

                    <form method="post" id="form_update_generator" action="{{ url('admin/generatorUpdate') }}">

                        <div class="value-generator-div">
                            <input type="text" placeholder="Custo do Gerador por KWh" name="value_generator"
                                id="value_generator" value="{{ $value_generator }}">
                            <input type="hidden" name="_token" id="token"
                                value="<?php echo csrf_token(); ?>">

                            <input class="btn" type="submit" value="Atualizar">
                        </div>
                    </form>
                </div>

            </div>


    </div>
    @endif


    </body>



    <div class="modal fade" id="modalInsert">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="myModalLabel">Adicionar Estado</h3>
                </div>
                <form id="form_add_state" method="post" action="{{ url('admin/addState') }}">

                    <div class="modal-body" style="text-align: center;">


                        <input type="text" placeholder="Sigla" name="state" id="state">
                        <input type="number" step="0.01" placeholder="Valor KWH" name="value_kwh" id="value_kwh">


                        <input type="hidden" name="_token"
                            value="<?php echo csrf_token(); ?>">
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Adicionar</button>
                    </div>

                </form>
            </div>
        </div>
    </div>



    <div class="modal fade" id="basicModalDelete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="myModalLabel">Excluir Estado</h3>
                </div>
                <form method="post" id="form_delete" action="{{ url('admin/deleteState') }}">
                    <div class="modal-body" style="text-align: center">

                        <h4>Tem certeza que deseja excluir?</h4>
                        <input type="text" hidden name="id_state" id="id_state">

                        <input type="hidden" name="_token"
                            value="<?php echo csrf_token(); ?>">
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Excluir</button>
                    </div>

                </form>
            </div>
        </div>
    </div>



    <div class="modal fade" id="largeModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="myModalLabel">Editar Estado</h3>
                </div>
                <form method="post" id="form_update_state" action="{{ url('admin/updateState') }}">
                    <div class="modal-body" style="text-align: center">


                        <input type="text" hidden name="id_state_update" id="id_state_update">
                        <input type="text" placeholder="Sigla" name="state_update" id="state_update">
                        <input type="number" step="0.01" placeholder="Valor KWH" name="value_kwh_update"
                            id="value_kwh_update">

                        <input type="hidden" name="_token"
                            value="<?php echo csrf_token(); ?>">


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-default">Editar</button>

                    </div>
                </form>
            </div>
        </div>
    </div>

    </html>




@endsection
