@extends('main')
@section('content')

<div class="container-full flex-column admin">
    <div class="flex-column container login">
        
            
                <div class="option table-states title-admin">
                    <h1>Login</h1>
                    <p> Faça login como administrador</p>
                </div>

                <div class="option value-generator-div">
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div style="display: flex; flex-direction: column; padding: 10px;" class="{{ $errors->has('email') ? ' has-error' : '' }}">

                            <input placeholder='E-mail' id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            

                            <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                            
                                <input placeholder="Senha" id="password" type="password" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            
                            </div>
                            <input type="submit" class="btn-green" value="Login">
                        </div>

                        

                                

                    </form>
                </div>
            
        
    </div>
</div>

@endsection
