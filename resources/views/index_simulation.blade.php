@extends('main')
@section('content')
        <div class="container-full flex-column admin">
            <div class="flex-column container">
            
                <div class="option table-states title-admin">
                    <h1>Painel Solar</h1>
                    <p> Gerencie e insira novos estados</p>
                </div>
                <div class="option value-generator-div">
                    <form method="post" id="form_simulation" action="{{url('solar_energy/generator')}}">
                    
                    <div style="display: flex; flex-direction: column; padding: 10px;">
                        <input type="text" required placeholder="Nome" name="name" id="name">
                        <input type="email" required placeholder="E-mail" name="email" id="email">
                        <input type="text" required placeholder="Endereço" name="address" id="address">
                        <input type="number" required placeholder="Telefone" name="phone" id="phone">
                    
                        <select name="state" id="state">
                        @foreach ($data as $state)
                            <option value="{{$state->id}}">{{$state->state}}</option>
                        @endforeach
                        </select>
                        <input type="number" required step="0.01" placeholder="Valor Mensal da conta de energia" name="value_kwh" id="value_kwh">
                        <input type="hidden" name="_token" id="token" value="<?php echo csrf_token(); ?>">

                        <input class="btn" type="submit" value="Consultar">
                    </div>
                </form>

            </div>    
        </div> 
    </body>
</html>

  
    <!-- basic modal -->
    <div class="modal fade modal-result-simulation" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title" id="myModalLabel">Resultado Simulação</h3>
            </div>
            <div class="modal-body" style="text-align: center;">
                <p> O Custo do gerador solar será de: </p>
                <h4 id="result_simulation"> </h4>
                <p>Tempo para compensar em meses: </p> 
                <h4 id="result_time"></h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Nova Consulta</button>
              
            </div>
          </div>
        </div>
    </div>


@endsection