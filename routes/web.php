<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });


Route::get('/', 'AdminController@index')->middleware();
/* rotas cliente */
Route::get('solar_energy/index_simulation', 'SolarEnergyController@index')->middleware();
Route::post('solar_energy/generator', 'SolarEnergyController@generator')->middleware();
// Route::get('solar_energy/index_admin', 'SolarEnergyController@indexAdmin')->middleware();

/* rotas admin */
Route::get('admin/index', 'AdminController@indexAdmin')->middleware();
Route::post('admin/addState', 'AdminController@addState')->middleware();
Route::post('admin/updateState', 'AdminController@updateState')->middleware();
Route::post('admin/generatorUpdate', 'AdminController@generatorUpdate')->middleware();
Route::post('admin/deleteState', 'AdminController@deleteState')->middleware();

/* rotas login */
Auth::routes();
Route::get('/home', 'AdminController@index')->middleware();
