<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Admin extends Model
{
    protected $table = 'states';
    public $timestamps = false;

    public function getAdmin(){
        $admin = new Admin();
        $result = $admin->orderBy('id', 'asc')->get();
        return $result;
    }

    public function getValueState($id_state){
        $admin = new Admin();
        $result = $admin->where('id', $id_state)->get();
        return $result;    
    }

    public function addState($state, $value_kwh){
        $state_insert = new Admin();
        $state_insert->state = $state;
        $state_insert->value_kwh = $value_kwh;
        
        if ($state_insert->save()) {
            return [
                'id' => $state_insert->id
            ];
        } else {
            return false;
        }
    }

    public function editState($state_id, $state_name, $value){
        $state = new Admin();
        $result = $state->where('id', $state_id)
                        ->update(['state' => $state_name,
                                    'value_kwh' => $value ]);

        return $result;
    }

    public function deleteState($state_id){
        $state = new Admin();
        $result = $state->where('id', $state_id)->delete();
        return $result;

    }
}
