<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolarEnergy extends Model
{
    protected $table = 'simulations';
    public $timestamps = false;

    public function addSimulation($name, $email, $phone, $address, $state, $value_kwh, $value_generator){

        $solar_energy = new SolarEnergy();
        $solar_energy->name = $name;
        $solar_energy->email = $email;
        $solar_energy->phone = $phone;
        $solar_energy->address = $address;
        $solar_energy->state = $state;
        $solar_energy->value_kwh = $value_kwh;
        $solar_energy->value_generator = $value_generator;

        if ($solar_energy->save()) {
            return true;
        } else {
            return false;
        }

    }

    public function getSimulations() {
        $solar_energy = new SolarEnergy();
        $result = $solar_energy->get();
        return $result;
    }

}
