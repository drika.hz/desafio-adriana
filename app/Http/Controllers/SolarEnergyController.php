<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SolarEnergy;
use App\Admin;
use App\User;

class SolarEnergyController extends Controller
{

    public function index(){
        $admin_model = new Admin();
        $result['data'] = $admin_model->getAdmin();
        return view('index_simulation', $result);
    }

    public function generator() {
        $state_model = new Admin();
        $solar_model = new SolarEnergy();
        $user_model = new User();

        $name = request()->input('name');
        $email = request()->input('email');
        $phone = request()->input('phone');
        $address = request()->input('address');

        $state = request()->input('state');
        $value_kwh = request()->input('value_kwh');
        

        if($name == '' || $email == '' || $phone == '' || $address == '' || $value_kwh == '' ){
            $data['status'] = 'Campos Obrigatórios';
        }else{

            $value_kwh_format = str_replace(',', '.', $value_kwh);

            //busca valor do kwh do estado selecionado
            $state_value = $state_model->getValueState($state);
            
            $kwh_monthly = $value_kwh/$state_value[0]->value_kwh;

            // busca o custo do gerador no banco para o cálculo
            $value_generator = $user_model->getValueGenerator() * $kwh_monthly;
            $value_generator_format = number_format($value_generator, 2, ',', '.');
            
            $time = $value_generator/$value_kwh;
            $time_format = number_format($time, 2, '.', '');

            $data['value_time'] = $time_format;
            $data['value_generator_format'] = $value_generator_format;
            $data['status'] = 'success';

            // adiciona na tabela de simulações
            $state_value = $solar_model->addSimulation($name, $email, $phone, $address, $state_value[0]->state, $value_kwh, $value_generator);
            
        }
        
        return response()->json($data);
    
    }

    
}
