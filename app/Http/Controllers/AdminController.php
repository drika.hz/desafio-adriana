<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\SolarEnergy;
use App\User;

class AdminController extends Controller
{
    
    public function index() {
        
        return view('index');
    }


    public function indexAdmin(){
        $admin_model = new Admin();
        $solar_model = new SolarEnergy();
        $user_model = new User();
        $result['status'] = 'success';
        $result['data'] = $admin_model->getAdmin();
        $result['data_simulations'] = $solar_model->getSimulations();
        $result['value_generator'] = $user_model->getValueGenerator();
        return view('index_admin', $result);
    }

    public function addState(){
        $admin_model = new Admin();
        $state = request()->input('state');
        $value_kwh = request()->input('value_kwh');

        if($state == '' || $value_kwh == ''){
            $result['status'] = 'Campos Obrigatórios';
        }else{
            $value_kwh = str_replace(',', '.', $value_kwh);
            $value_kwh_format = number_format($value_kwh, 2, '.', '');
            $result = $admin_model->addState($state, $value_kwh_format);
            if($result){
                $result['status'] = 'success';
            }else{
                $result['status'] = 'error';
            }
        }

        return response()->json($result);
            
    }

    public function updateState(){
        $admin_model = new Admin();
        $id = request()->input('id_state_update');
        $state = request()->input('state_update');
        $value_kwh = request()->input('value_kwh_update');

        if($state == '' || $value_kwh == ''){
            $data['status'] = 'Campos Obrigatórios';
        }else{
            $value_kw = str_replace(',', '.', $value_kwh);
            $value_kwh_format = number_format($value_kw, 2, '.', '');
            $result = $admin_model->editState($id, $state, $value_kwh_format);

            if($result){
                $data['status'] = 'success';
            }else{
                $data['status'] = 'error';
            }

        }
        return response()->json($data);
    }

    public function deleteState(){
        $admin_model = new Admin();
        $id = request()->input('id_state');
        $result = $admin_model->deleteState($id);
        if($result){
            return $this->indexAdmin();
        }else{
            return 'erro';
        }
    }

    public function generatorUpdate(){
        $user_model = new User();
        $value_generator = request()->input('value_generator');
        if($value_generator == ''){
            $data['status'] = 'Campos Obrigatórios';
        }else{
            $value_generator = str_replace(',', '.', $value_generator);
            $value_kwh_format = number_format($value_generator, 2, '.', '');
            $result = $user_model->generatorUpdate($value_kwh_format);
        
      
            $data['status'] = 'success';
        }
        return response()->json($data);
        
    }
}
