<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Generator extends Model
{
    protected $table = 'generator';
    public $timestamps = false;

    public function getGenerator(){
        $generator = new Generator();
        $result = $generator->get();
        return $result;
    }

    public function editGenerator($id, $value){
        $generator = new Generator();
        $result = $generator->where('id', $id)->update(['value_kwh' => $value ]);
        return $result;
    }
}
