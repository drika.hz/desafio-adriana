<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getValueGenerator(){
        $user = new User();
        $result = $user->where('name', 'admin')->get();
        return $result[0]->value_kwh;    
    }

    public function generatorUpdate($value){
        $user = new User();
        $result = $user->where('name', 'admin')
                        ->update(['value_kwh' => $value ]);

        return $result;
    }
}
