-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 28-Jun-2021 às 00:48
-- Versão do servidor: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `desafiobd`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `simulations`
--

CREATE TABLE `simulations` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` bigint(20) NOT NULL,
  `address` text NOT NULL,
  `state` varchar(50) NOT NULL,
  `value_kwh` float NOT NULL,
  `value_generator` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `simulations`
--

INSERT INTO `simulations` (`id`, `name`, `email`, `phone`, `address`, `state`, `value_kwh`, `value_generator`) VALUES
(58, 'João Silva', 'joaosilva@email.com', 48991265485, 'Rua José Bonifácio, 654', 'RS', 220, 86.2745),
(59, 'Maria Oliveira', 'maria@email.com', 4965412345, 'Rua Imperatriz 122', 'AM', 100, 21.978);

-- --------------------------------------------------------

--
-- Estrutura da tabela `states`
--

CREATE TABLE `states` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `state` text NOT NULL,
  `value_kwh` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `states`
--

INSERT INTO `states` (`id`, `state`, `value_kwh`) VALUES
(43, 'RS', 0.51),
(46, 'GO', 0.46),
(50, 'BH', 0.45),
(51, 'AM', 0.91),
(52, 'SC', 0.11);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `updated_at` text NOT NULL,
  `created_at` text NOT NULL,
  `remember_token` text NOT NULL,
  `value_kwh` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `updated_at`, `created_at`, `remember_token`, `value_kwh`) VALUES
(2, 'admin', 'admin@admin.com', '$2y$10$9dzEfGadP1kO6PKG.bLi6ep3TPUKfzA/q69NvHaxrADOjeozqr.TG', '2021-06-27 17:27:03', '2021-06-27 11:31:57', 'Ddw0b5c15b7b8nf7dnZnGRr6b7xAGJOY8ShXJBtKrEGoAcvxuJJ9U1gOmVLU', 0.2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `simulations`
--
ALTER TABLE `simulations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `simulations`
--
ALTER TABLE `simulations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
